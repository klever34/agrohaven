//axios GET POST PUT etc...
import axios from 'axios';
const localStorage = require("nativescript-localstorage");
const token = localStorage.getItem('token');
export default {
    login: async function(loginData) {
        const response = await axios.post('/Account/Login', loginData);
        return response;
    },
    signUp: async function(regData) {
        
    },
    createDeliverables: async function(deliverablesData) {
        const response = await axios.post('/Deliverables/Create', deliverablesData);
        return response;
    },
    loadInitializationData: async function(fetchData) {
        const response = await axios.post('/Deliverables/FetchInitializationData', fetchData);
        return response;
    },
    getAllDeliverables: async function(reloadData) {
        const response = await axios.post('/Deliverables/Reload', reloadData);
        return response;
    }
}