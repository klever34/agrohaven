import Vue from 'nativescript-vue';
import Vuex from 'vuex';

import agrohaven from './modules/agrohaven';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

const store = new Vuex.Store({
  modules: {
    agrohaven,
  },
  strict: (TNS_ENV === 'debug'),
});

Vue.prototype.$store = store;

export default store;
