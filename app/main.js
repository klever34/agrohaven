import Vue from 'nativescript-vue';
import Walkthrough from './components/Walkthrough';
import Login from './components/Login';
// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = (TNS_ENV === 'production')

Vue.registerElement('AnimatedCircle', () => require('nativescript-animated-circle').AnimatedCircle);
Vue.registerElement('CardView', () => require('nativescript-cardview').CardView);
Vue.registerElement('Carousel', () => require('nativescript-carousel').Carousel);
Vue.registerElement('CarouselItem', () => require('nativescript-carousel').CarouselItem);
Vue.registerElement('CheckBox', () => require('nativescript-checkbox').CheckBox);
Vue.registerElement("DropDown", () => require("nativescript-drop-down").DropDown);
Vue.registerElement("NumberProgressBar", () => require("nativescript-number-progressbar").NumberProgressBar);
Vue.registerElement("exoplayer", () => require("nativescript-exoplayer").Video);
Vue.registerElement('Fab', () => require('nativescript-floatingactionbutton').Fab);
Vue.registerElement("Gradient", () => require("nativescript-gradient").Gradient);
Vue.registerElement('RadSideDrawer', () => require('nativescript-ui-sidedrawer').RadSideDrawer);
Vue.registerElement('StarRating', () => require('nativescript-star-ratings').StarRating);
require("nativescript-ui-chart/vue");
require("nativescript-ui-gauge/vue");
Vue.registerElement("PreviousNextView", () => require("nativescript-iqkeyboardmanager"). PreviousNextView)
import RadChart from 'nativescript-ui-chart/vue';

Vue.use(RadChart);

import axios from 'axios';
axios.defaults.baseURL = 'https://agrohaven.agrisatfarm.com';

import router from './router';

import store from './store';


new Vue({
  router,
  store,
  template: `<Frame><router-view/></Frame>`,
}).$start()
