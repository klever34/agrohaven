const state = {
  header: "",
  selectedTab: "",
};

const mutations = {
  setHeader (state, header) {
    state.header = header;
  },
  setTab (state, tab) {
    state.selectedTab = tab;
  },
};

const actions = {

};

export default {
  state,
  mutations,
  actions,
};
