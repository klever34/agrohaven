import Vue from 'nativescript-vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import Home from '../components/Home';
import Signup from '../components/Signup';
import Dashboard from '../components/Dashboard';
import Login from '../components/Login';
import Walkthrough from '../components/Walkthrough';
import guard from './authGuard';



const router = new VueRouter({
  routes: [
    {
      path: '/home',
      name: 'Home',
      component: Home,
      meta: {
        title: 'Home',
        isAuthenticated: true
      },
    },
    {
      path: '/login',
      component: Login,
      meta: {
        title: 'Login',
        // isAuthenticated: true
      },
    },
    {
      path: '/signup',
      name: 'SignUp',
      component: Signup,
      meta: {
        title: 'Sign Up',
      },
    },
    {
      path: '/dash',
      name: 'Dash',
      component: Dashboard,
      meta: {
        title: 'DashBoard',
        isAuthenticated: true
      },
    },
    {
      path: '/walkthrough',
      name: 'Walkthrough',
      component: Walkthrough,
      meta: {
        title: 'Walkthrough',
        isAuthenticated: true
      },
    },
    {path: '*', redirect: '/home'},
  ],
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.isAuthenticated)) {
      guard.isAuthenticated(to, from, next)
      return;
  } 
  if(to.matched.some(record => record.meta.isUserInSession)) {
      guard.isUserInSession(to, from, next)
      return;
  }
  next();
});

router.replace('/home');

export default router;
