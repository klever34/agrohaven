  import * as application from "application";
  import {
      AndroidApplication
  } from "application";

  export function mapBuilder(obj) {
      const map = new Map();
      Object.keys(obj).forEach(key => {
          map.set(key, obj[key]);
      });
      return map;

  }

  export function checkInternet() {
      const connectivityModule = require("tns-core-modules/connectivity");
      const connectionType = connectivityModule.getConnectionType();

      switch (connectionType) {
          case connectivityModule.connectionType.none:
              console.log("No connection");
              return false;
              break;
          case connectivityModule.connectionType.wifi:
              console.log("WiFi connection");
              return true;
              break;
          case connectivityModule.connectionType.mobile:
              console.log("Mobile connection");
              return true;
              break;
          default:
              break;
      }
  }

  export function onBackpressed() {
      const platformModule = require("tns-core-modules/platform");
      if (platformModule.isAndroid) {
          application.android.on(AndroidApplication.activityBackPressedEvent, (data) => {
              console.log("back button pressed");
              application.android.foregroundActivity.finish();
          });
      } else if (platformModule.isIOS) {

      }
  }

  export function setCountdownTimer(minutes, seconds) {
      let endTime, gethours, getmins, minutesLeft, time;
      function updateTimer() {
        minutesLeft = endTime - (+new Date);
          if (msLeft < 1000) {} else {
              time = new Date(minutesLeft);
              gethours = time.getUTCHours();
              getmins = time.getUTCMinutes();
              setTimeout(updateTimer, time.getUTCMilliseconds() + 500);
          }
      }
      endTime = (+new Date) + 1000 * (60 * minutes + seconds) + 500;
      updateTimer();
  }